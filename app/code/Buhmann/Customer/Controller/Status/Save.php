<?php
/**
 * Copyright © Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Buhmann\Customer\Controller\Status;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Customer\Model\Session;

class Save extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $session;

    /**
     * @var Validator
     */
    protected $formKeyValidator;

    /**
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param Session $customerSession
     * @param Validator $formKeyValidator
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        Session $customerSession,
        Validator $formKeyValidator
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->session = $customerSession;
        $this->formKeyValidator = $formKeyValidator;
        parent::__construct($context);
    }

    public function execute()
    {
        $post = $this->getRequest()->getPost();
        $customer = $this->session->getCustomer();
        $data = [];

        if ($customer && $this->getRequest()->isPost()){
            try {
                $data['customer_status'] = $post['customer_status'];

                $customerData = $customer->getDataModel();
                $customerData->setCustomAttribute('customer_status', (string) $post['customer_status']);
                $customer->updateData($customerData);
                $customer->save();

                $this->messageManager->addSuccess(__('Status has been saved.'));
            } catch (\Exception $e) {
                $this->messageManager->addError(__('Something went wrong.'));
            }
        }

        $resultJson = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        $resultJson->setData($data);

        return $resultJson;
    }
}
