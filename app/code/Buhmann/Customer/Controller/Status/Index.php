<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Buhmann\Customer\Controller\Status;

class Index extends \Buhmann\Customer\Controller\Status
{
    /**
     * Managing Status page
     *
     * @return void
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->getPage()->getConfig()->getTitle()->set(__('Status'));
        $this->_view->renderLayout();
    }
}
