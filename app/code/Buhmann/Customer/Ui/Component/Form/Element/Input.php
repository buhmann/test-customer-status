<?php
/**
 * Copyright © Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Buhmann\Customer\Ui\Component\Form\Element;

use Magento\Framework\View\Element\UiComponentInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponent\DataSourceInterface;
use Magento\Customer\Model\Session;

class Input extends \Magento\Ui\Component\Form\Element\Input
{
    /**
     * Render context
     *
     * @var ContextInterface
     */
    protected $context;

    /**
     * @var UiComponentInterface[]
     */
    protected $components;

    /**
     * @var array
     */
    protected $componentData = [];

    /**
     * @var DataSourceInterface[]
     */
    protected $dataSources = [];

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $session;

    /**
     * Constructor
     *
     * @param ContextInterface $context
     * @param UiComponentInterface[] $components
     * @param array $data
     * @param Session $customerSession
     */
    public function __construct(
        ContextInterface $context,
        array $components = [],
        array $data = [],
        Session $customerSession
    ) {
        $this->context = $context;
        $this->components = $components;
        $this->_data = array_replace_recursive($this->_data, $data);
        $this->initObservers($this->_data);
        parent::__construct($context, $components, $this->_data);
        $this->session = $customerSession;
    }

    /**
     * Prepare component configuration
     *
     * @return void
     */
    public function prepare()
    {
        parent::prepare();
        $config = $this->getData('config');
        $customer = $this->session->getCustomer();

        if(isset($config['dataScope']) && $config['source'] == 'customer'){
            $config['default']= $customer->getData($config['dataScope']);
            $this->setData('config', (array)$config);
        }
    }
}
