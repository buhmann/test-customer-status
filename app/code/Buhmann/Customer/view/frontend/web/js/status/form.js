/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'Magento_Ui/js/form/form',
    'Magento_Customer/js/customer-data'
], function($, Component, customerData){
    'use strict';

    return Component.extend({
        initialize: function () {
            this._super();

            this.responseStatus.subscribe($.proxy(function(status){
                if(status){
                    customerData.reload('customer');
                }
            }, this));
        },
        submit: function (redirect) {
            this._super(true);
        },
    });
});
