<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Buhmann\Customer\Plugin\CustomerData;

use Magento\Customer\Helper\Session\CurrentCustomer;

/**
 * Cart source
 */
class Customer
{
    /**
     * @var CurrentCustomer
     */
    protected $currentCustomer;

    /**
     * @param CurrentCustomer $currentCustomer
     * @param View $customerViewHelper
     */
    public function __construct(
        CurrentCustomer $currentCustomer
    ) {
        $this->currentCustomer = $currentCustomer;
    }

    /**
     * {@inheritdoc}
     */
    public function afterGetSectionData(
        \Magento\Customer\CustomerData\Customer $subject,
        $result
    ){
        $data = $result;
        if(!empty($data)){
            $customer = $this->currentCustomer->getCustomer();
            $data['customer_status'] = $customer->getCustomAttribute('customer_status')->getValue();
        }
        return $data;
    }
}
